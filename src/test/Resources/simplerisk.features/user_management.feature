Feature: User Management

  User Story: (optional)
  In order to manage users
  As an administrator
  I need CRUD permissions to user accounts

  Rules:
  - Non-admin users have have no permissions over other accounts
  - There are two types of non-admin accounts
  -- Risk Managers
  -- Asset Managers
  - Non-admin users must only be able to see risks or assets in their groups
  - The administrator must be able to reset user's passwords
  - When an account is created, a user must be forced to reset their password on first login

  Questions:
    - Do admin need access to all accounts or only to the acounts in their group?

  To do:
  - Force reset of password on account creation

  Domain language:
  Group = users are defined by which group they belong to.
  CRUD = Create, Read, Update, Delete
  administrator permissions = access to CRUD risks, users and assets for all groups
  risk_mangement permissions = only able to CRUD risks
  asset_mangement permisions = only able to CRUD assets

  Background:
    Given a user called simon with administrator permissions and password S@feB3ar
    And a user called tom with risk_management permissions and password S@feB3ar

    @high-impact
    Scenario Outline: The administrator checks a user's details
      When simon is logged in with password S@feB3ar
      Then he is able too view <user>'s account
      Examples:
        | user |
        | tom  |

  @high-risk
  @to-do
    Scenario Outline: A user's password is reset by the adminstrator
    Given a <user> has lost his password
    When simon is logged in with password S@feB3ar
    Then Simon can reset <user>'s password
    Examples:
      | user |
      | tom  |



